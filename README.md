# Model compilation setup Guide

# Introduction
This document guides the developers on “How to compile the Deep Learning model with TVM Compiler”. The model is compiled from ONNX to TVM compiled model.The compiled model is to demonstrate the capabilities of the ARM platform to run heavy deep learning inference on low-cost CPU of ARM. To demonstrate this, the YoloV3 based object detection is implemented to solve the traffic monitoring use case and is deployed on the low-cost ARM Based CPU.

After the model has been trained, the user can use this repository for compiling the model and inferencing with the updated model. See **ml_model > training** README documents first.

## Pre-Requisites
- ARM Based Embedded Platform. The following are recommended:
    - Rockchip 3399ProD
    - NXP i.MX 8M Plus
- USB Pen Drive Flashed with ARM System-Ready EWAOL Image with preinstalled docker.
- Setup of the Alibaba Cloud service Account.
- Setup of the **Object Storage Service** on Alibaba Cloud

## Overview
- Clone the project
- Building model the compilation docker container and storing the compiled model on Alibaba Cloud.
  - Setup Alibaba account
    - Creating RAM (Resource Access Management) User.
    - Creating OSS (Object Storage Service) Bucket
  - Build and run the container.
- Building model the compilation docker container and storing the compiled model on the local host.
- What's next ?

The user can compile the model and store the model locally or storing it on Alibaba Cloud. Choose between those 2 sections.

## Clone the project
Run the command given below to clone the project.
```
$ git clone git@gitlab.com:Linaro/blueprints/software-defined-camera/reference-use-cases/traffic-monitoring/ml-model/compilation.git
```
## Building model the compilation docker container and storing the compiled model on Alibaba Cloud
### Creating RAM (Resource Access Management) User
A Resource Access Management (RAM) user is an entity created on Alibaba Cloud to allow the end user to interact with Alibaba Cloud Services and authorize different access and permissions. Please Follow the below steps to set up the RAM.

- Open the Alibaba Cloud Page & Login with the Account Created.
- Search for **Resource Access Management** on Alibaba Cloud console. Access this service.
- On the left-side navigation pane, choose **Identities > Users**.
- On the **Users** page, click on the **Create User** button.
  - In the **User Account Information** section, enter the **Logon Name** and **Display Name**.
  - In the **Access Mode** section, select both of the access mode mentioned below:
    - **Console Access**. Select this option and complete the logon security settings.
      - Under **Set Logon Password**, select **Automatically Regenerate Default Password**.
      - Under **Password Reset**, select **Not Required**.
      - Under **Enable MFA** (multi-factor authentication), select **Not Required**.
    - **OpenAPI Access**
      - Upon selection, an AccessKey pair is automatically created for the RAM user.
      - The RAM user can call API operations or use other development tools to access Alibaba Cloud resources.

  - After configuring the Create User form, Click on **OK**.
    - Then, on the user created page, you can go to **User AccessKeys** section, click on the **Create AccessKey** button and download the CSV file.
- On the **Users** page, in the **Actions** column of the newly created user, click on **Add Permissions**.
  - Under **Select Policy** section, in the “Enter policy name” search box, type **AliyunOSSFullAccess**. Select the policy and then click the **OK** button.
  - Click on **Complete**.

### Creating OSS (Object Storage Service) Bucket
Object Storage Service (OSS) stores data as objects within buckets. Please Follow the below steps to setup the OSS.
- Search for **Object Storage Service** on Alibaba Cloud console. Access this service.
- In the left-side navigation pane, click on **Buckets** and then, click on **Create Bucket**.
- In the **Create Bucket** panel, configure the required parameters like the name of the bucket, and the region.
  - Select the preferred region for the OSS bucket. In our case, we have used China (Shanghai) region.

    Let the remaining configuration to its default values.

  - Click on **OK**. This bucket is getting prepared for storing the compiled model by TVM.

### Build and run the container
The steps below allow the user to build the docker container for compiling the custom YoloV3 and tiny yolov3 Object Detection model into the TVM Compiled model format. The Model compilation is the most important aspect to consider when users want to deploy the model on the low-cost, low-power embedded platform. Model compilation reduces unwanted operations, optimizes the math ops, optimizes the model graph, memory mapping & cache optimization etc.

The docker container downloads the model from the OSS bucket and stores the final model into the OSS bucket after compilation.

- Copy the project source to the target device (e.g. RK3399/IMX8MP).
- Open the config file located at **config/alibaba_config.json** in an editor. Update the configuration parameters given below:
  - ALIBABA_ACCESS_KEY – can be found in the CSV file downloaded while
creating RAM user.
  - AIBABA_SECRET_KEY – can be found in CSV file downloaded while
creating RAM user.
  -  ALIBABA_REGION_ID – Give the region used while creating the OSS bucket.
     - Refer to the [Supported regions](https://www.alibabacloud.com/help/en/iot-platform/latest/regions) page for getting the proper region id.



- Run the commands given below from the root of the project directory from RK3399ProD or i.MX8M Plus to build the container.
```sh
cd <PROJECT_ROOT>
docker build -t <IMAGE_NAME>:<TAG_NAME> .
```
  Users can specify the IMAGE_NAME & TAG_NAME of their choice. IMAGE_NAME is the name of the docker image. TAG_NAME is to tag the different variants of the same image.

  **Note**: This step takes 4.5 to 5 hours of time to build the image.

- The above step may take a while. Once the docker image is built, the next step
is to run the container. Run the below command to run the container.
```sh
docker run -it --network=host <IMAGE_NAME>:<TAG_NAME> -p
alibaba_cloud -ib <INPUT_BUCKET_NAME> -ob <OUTPUT_BUCKET_NAME> -if <INPUT_FILENAME> -of <OUTPUT_FILENAME>
```

- Arguments:
  - -p: Taking the model locally or from Alibaba cloud. If locally, use “local” if Alibaba cloud, use “alibaba_cloud”.
  - -ib: Input bucket name. Created while training the model & contains the latest trained model.
  - -ob: Output bucket name. Generated by the user in the above steps.
  - -if: Path of the ONNX Model file inside the bucket. This file is generated during model training step.  
  - -of: Path for TVM Compiled model to store in output bucket 
    Note: make sure that name contains the “.tar.gz” extension.
- An example command to run this docker image would be
```sh
$ docker run --network=host \  
    -v $PWD/trained_model:/trained_model/ 
    compilation:v1 -p alibaba \  
    -if trainedmodel0623_onnx/trained_model.onnx \  
    -of trainedmodel0623_onnx/compiled_model.tar.gz \  
    -ib greengrass-orchestration -ob greengrass-orchestration
```
Once the execution of the docker run is complete, it generates the compiled model inside the output bucket which is given by the user in config.

Notes:
- Make sure that your internet speed is good.
- The user needs to download the model and use it when building the inference container (cn-inference).

## Building model of the compilation docker container and storing the compiled model on local host
The user can compile the model and store the model locally.
TVM compilation compiles the ONNX model to TVM and stores it in a container. Once the user clicks on the download link, the model is downloaded on the local system.

- Copy the cloned project folder (on Host) to iMX8MP or RK3399ProD device using the command given below.
```sh
scp -r <PROJECT_FOLDER> root@<ip_address_of_device>:~
```
- Login to the shell of the RK3399Pro D or i.MX8M Plus with the command given below.
```sh
ssh root@<ip_address_of_device>
```

Follow the steps below to build the container in the RK3399ProD or i.MX 8M Plus device. Run the commands given below from the root of the project directory from RK3399ProD or i.MX8M Plus.
- To build the docker container.
```sh
cd <PROJECT_FOLDER>
docker build -t <IMAGE_NAME>:<TAG_NAME> .
```
  Users can specify the IMAGE_NAME & TAG_NAME of their choice. IMAGE_NAME is the name of the docker image. TAG_NAME is to tag the different variants of the same image.

  **Note**: This step takes 4.5 to 5 hours of time to build the image.

- The above step may take some time. Once the docker image is built, the next step is to run the container.
  - Create a folder named **trained_model/** and copy the trained model into that folder.
  - Run the below command to compile the trained model.
```sh
cd <PROJECT_ROOT_FOLDER>/
```
```sh
mkdir trained_model/
```
```sh
cp <TRAINED_MODEL_FILE_PATH> trained_model/
```
```sh
docker run -it --network=host \
    –v $PWD/trained_model/:/trained_model/  \
    <IMAGE_NAME>:<TAG_NAME> -p local \
    -if <INPUT_FILENAME> -of <OUTPUT_FILENAME>
```
- Arguments:
  - -p: Taking models locally or from Alibaba cloud. If locally use “local” if Alibaba cloud use “alibaba_cloud”.
  - -if: Name of the ONNX Model file. Generated during model
training step.
  - -of: Name for TVM Compiled model (Make sure that name must contain “.tar.gz” extension)
- An example command to run the docker file would be 
```sh
$ docker run --network=host \  
    -v $PWD/trained_model:/trained_model/ compilation:v1 \ 
    -p local -if tiny_stage2_opt.onnx -of compiled_model.tar.gz 
```
- After compilation the docker container will provide us the download link. Copy and paste in browser to download the model.
- This compiled model will be used in building the inference container. Copy the newly compiled model to model_data folder of inference project folder and build the
inference container.

## Building model compilation docker container and storing the compiled model on AWS Cloud.
The docker container downloads the model from S3 bucket and stores the final model into S3 bucket after compilation.

- Copy the project source to the target device (RK3399/IMX8MP)
- Open config file located at config/aws_config.json in an editor.
- Update the configuration parameters given below in alibaba_config.json,
  - access_key – can be found in CSV file downloaded while
creating IAM user.
  - secret_key – can be found in CSV file downloaded while
creating IAM user.
  - region – Give the preferred region id.

### Build the container
- Run the commands given below from the root of the project directory from RK3399ProD or i.MX8M Plus.
```
$ cd <PROJECT_ROOT>
$ docker build -t <IMAGE_NAME>:<TAG_NAME> .
```

  - IMAGE_NAME, is name of the docker image.
  - TAG_NAME, is to tag the different variant of same image.

Users can specify the IMAGE_NAME & TAG_NAME of their choice.
(Note: This step takes 4.5 to 5 hours of time to build the image)

- The above step may take a while. Once the docker image is built, the next step
is to run the container. Run the below command to run the container.
```
$ docker run -it --network=host <IMAGE_NAME>:<TAG_NAME> -p
aws -ib <INPUT_BUCKET_NAME> -ob <OUTPUT_BUCKET_NAME> -if <INPUT_FILENAME> -of <OUTPUT_FILENAME>
```

- Arguments:
  - -p: Taking model locally or from AWS cloud. If locally use “local”, if Alibaba cloud use “alibaba_cloud” , if AWS cloud use “aws”.
  - -ib: Input bucket name. Created while training the model & contains the latest trained model. 
  - -ob: Output bucket name. Generated by the user in the above steps.
  - -if: Path of the ONNX Model file inside the bucket. This file is generated during model training step
  - -of: Path for TVM Compiled model to store in output bucket.
      (Make sure that name must contain “.tar.gz” extension)
- An example command for running the docker image would be  
```sh
$ docker run --network=host \  
    -v $PWD/trained_model:/trained_model/ 
    compilation:v1 -p aws \  
    -if trainedmodel0623_onnx/trained_model.onnx \  
    -of trainedmodel0623_onnx/compiled_model.tar.gz \  
    -ib greengrass-orchestration -ob greengrass-orchestration 
 ```
Once the execution of the docker run is complete, it generates the compiled model inside the output bucket which is given by the user in config.
- Make sure that internet speed is good.
- The user needs to download the model and use it when building the inference container.
