"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import tarfile
import os

def export(folder, tarname):
	file = tarfile.open(tarname, "w|gz")
	o = os.listdir(folder)
	for i in o:
		print(i)
		file.add(folder+'/'+i)
	file.close()

