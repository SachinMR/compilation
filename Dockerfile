FROM arm64v8/ubuntu:20.04
FROM python:3.8-slim-buster

RUN apt-get update && \ 
    apt-get install -y python3 python3-dev python3-pip python3-setuptools python3-wheel \
                        gcc libtinfo-dev zlib1g-dev build-essential cmake libedit-dev libxml2-dev \
                        libgl1-mesa-glx libssl-dev git wget llvm clang libsm6 libxext6 
RUN apt install -y ocl-icd-opencl-dev
RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null
RUN apt-get install gpg -y
RUN echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ bionic main' | tee /etc/apt/sources.list.d/kitware.list >/dev/null
RUN apt-get update && rm /usr/share/keyrings/kitware-archive-keyring.gpg 
RUN apt-get install -y kitware-archive-keyring && apt-get install cmake ocl-icd-opencl-dev -y

WORKDIR /
RUN git clone --recursive -b v0.10.dev0 https://github.com/apache/tvm.git
WORKDIR /
RUN mkdir /tvm/build
RUN cp tvm/cmake/config.cmake /tvm/build
RUN echo "set(USE_LLVM ON)" >> /tvm/build/config.cmake

WORKDIR /tvm/build
RUN cmake ..
RUN make
ENV PYTHONPATH=/tvm/python:/tvm/topi/python:${PYTHONPATH}

RUN python3 -m pip install --upgrade pip

RUN python3 -m pip install boto3 oss2 flask 
RUN python3 -m pip install onnx onnxruntime  
RUN python3 -m pip install decorator scipy psutil attrs 
RUN python3 -m pip install tqdm

WORKDIR /
COPY src/ src/
COPY trained_model/ trained_model/
COPY config/ config/
COPY main.py main.py

ENTRYPOINT ["python3", "main.py"]
